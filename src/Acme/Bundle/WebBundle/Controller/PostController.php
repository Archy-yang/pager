<?php

namespace Acme\Bundle\WebBundle\Controller;

use Acme\Bundle\WebBundle\Entity\Post;
use Acme\Bundle\WebBundle\Form\PostType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class PostController extends Controller
{
    /**
     * @Template()
     */
    public function newAction()
    {
        $form = $this->createForm(new PostType);

        return [
            'form' => $form->createView(),
        ];
    }

    public function saveAction(Request $request)
    {
        $post = new Post;

        $form = $this->createForm(new PostType, $post);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($post);
            $em->flush();

            return $this->redirect($this->generateUrl('post_view', [
                'id' => $post->getId(),
            ]));
        }

        throw $this->createNotFoundException();
    }

    /**
     * @Template()
     */
    public function viewAction($id)
    {
        $post = $this->getDoctrine()->getRepository('AcmeWebBundle:Post')->find($id);

        if (null === $post) {
            return $this->createNotFoundException();
        }

        $form = $this->createForm(new PostType);

        return [
            'post' => $post,
            'form' => $form->createView(),
        ];
    }

    /**
     * @Template
     */
    public function listAction(Request $request, $offset = 0)
    {
        $page = $request->query->get('page');

        if (null !== $page) {
            $offset = ($page - 1) * 5;
        } else {
            $page = 1;
        }
        $em = $this->getDoctrine()->getManager();
        $sql = 'SELECT a FROM AcmeWebBundle:Post a ORDER BY a.id DESC';

        $lists = $em->createQuery($sql)->setFirstResult($offset)->setMaxResults(5)->getResult();
        $pages = $em->createQuery($sql)->getResult();
        $pageCount = ceil(count($pages)/5);

        $first = 1;
        $pre = $page - 1;
        $curr = $page;
        $nex = $page + 1;
        $last = $pageCount;

        return [
            'lists' => $lists,
            'pageCount' => $pageCount,
            'first' => $first,
            'pre' => $pre,
            'last' => $last,
            'nex' => $nex,
            'current' => $curr,
        ];
    }

}
